import os
import shutil


base_dir = 'D:\DB_cats_dogs'
original_data_set = 'D:\\DB_cats_dogs\\dogs-vs-cats\\train\\train'

train_dir = os.path.join(base_dir, 'train')

validation_dir = os.path.join(base_dir, 'validation')

test_dir = os.path.join(base_dir, 'test')

train_cats_dir = os.path.join(train_dir, 'cats')

train_dogs_dir = os.path.join(train_dir, 'dogs')

validation_cats_dir = os.path.join(validation_dir, 'cats')

validation_dogs_dir = os.path.join(validation_dir, 'dogs')

test_cats_dir = os.path.join(test_dir, 'cats')

test_dogs_dir = os.path.join(test_dir, 'dogs')


def fill_data_set(image_prefix: str, dst: str, start: int, finish: int):
    file_names = (f'{image_prefix}.{l}.jpg' for l in range(start, finish))
    for file_name in file_names:
        shutil.copyfile(os.path.join(original_data_set, file_name),
                        os.path.join(dst, file_name))


def prepare_all_data():
    os.mkdir(train_dir)
    os.mkdir(validation_dir)
    os.mkdir(test_dir)
    os.mkdir(train_cats_dir)
    os.mkdir(train_dogs_dir)
    os.mkdir(validation_cats_dir)
    os.mkdir(validation_dogs_dir)
    os.mkdir(test_cats_dir)
    os.mkdir(test_dogs_dir)

    fill_data_set('cat', train_cats_dir, 0, 1000)
    fill_data_set('cat', validation_cats_dir, 1000, 1500)
    fill_data_set('cat', test_cats_dir, 1500, 2000)

    fill_data_set('dog', train_dogs_dir, 0, 1000)
    fill_data_set('dog', validation_dogs_dir, 1000, 1500)
    fill_data_set('dog', test_dogs_dir, 1500, 2000)

    print('total train cat image', len(os.listdir(train_cats_dir)))


if __name__ == '__main__':
    prepare_all_data()
