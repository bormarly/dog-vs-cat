from keras.preprocessing import image
from keras import models
import numpy

model:models.Sequential = models.load_model('laba_cats_dogs.h5')
path_to_image = '11_cr.png'
image1 = image.load_img(path_to_image, target_size=(150, 150))
image_tenzor = image.img_to_array(image1)
image_tenzor = numpy.expand_dims(image_tenzor, axis=0)

result = model.predict(image_tenzor)
print(result)
