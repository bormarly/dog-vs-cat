## Класифікація зображень котів та собак
Під час тренування нейромережі датасет було розбита на 3 групи:

1. Тренування
2. Валідація
3. Тестування

![](/training_results/Training_and_validation_accuracy.jpg)
![](/training_results/Training_and_validation_loss.jpg)

##### Завантаження датасету
Потрібно завантажити архів за таким посиланням: https://www.kaggle.com/c/dogs-vs-cats/data

в файлі *prepare_data_set.py* 
Встановити значення змінної *original_data_set*
Шлях до датасету  
##### Налаштування датасету
`python prepare_data_set.py`
##### Навчання мережі 
`python study.py`
##### Використання мережі
Встановити в файлі *predict.py* значення змінної 
*path_to_image* шлях до зображення та виконати команду `python predict.py`
#### Результат
* (0)-кіт
* (1)-собака
